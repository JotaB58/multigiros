<?php

/* Mapear la url ingresada en el navegador,
1-controlador
2-metodo
3-parametro 
*/

class Core
{
	protected $controladorActual = 'Paginas';
	protected $metodoActual = 'index';
	protected $parametros = [];

	public function __Construct(){
		$url = $this ->getUrl();

//Buscar en controllers si el controlador existe
		if (file_exists('../app/controllers/' .ucwords($url[0]).'.php')) {
			$this-> controladorActual = ucwords($url[0]);

			//unset indice
			unset($url[0]);
		}

		//requerir el controlador
		require_once '../app/controllers/' . $this-> controladorActual . '.php';
		$this-> controladorActual = new $this-> controladorActual;

		if (isset($url[1])) {
			if (method_exists($this-> controladorActual, $url[1])) {
				$this-> metodoActual = $url[1];
				//unset indice
			unset($url[1]);
			}
		}

		$this-> parametros = $url ? array_values($url) : [];

		call_user_func_array([$this-> controladorActual, $this-> metodoActual],$this-> parametros);
		
	}

	public function getUrl(){
		//echo $_GET['url'];

		if (isset($_GET['url'])) {
			$url = rtrim($_GET['url'],'/');
			$url = filter_var($url,FILTER_SANITIZE_URL);
			$url = explode('/', $url);
			return $url;
		}
	}
}