<?php

/**
 Se encarga de cargar los modelos y las vistas 
 */
 class Controlador
 {
	//Cargar modelo
 	public function modelo($modelo){
 		require_once '../app/models/' . $modelo .'.php';
 		return new $modelo();
 	}

	//Cargar vista
 	public function vista($vista, $datos = []){
 		if (file_exists('../app/views/' . $vista . '.php')) {
 			require_once '../app/views/' . $vista . '.php';
 		}
 		else{
 			die("La vista no existe");
 		}		
 	}
 }